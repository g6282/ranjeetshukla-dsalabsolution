package com.gl.services;

import java.util.ArrayDeque;
import java.util.Deque;

public class VerifyBracketsBalanced {

	public boolean isBracketsBalanced(String bracketStr) {
		Deque<Character> stack = new ArrayDeque<Character>();

		// Traversing the Expression
		for (int i = 0; i < bracketStr.length(); i++) {
			char charExp = bracketStr.charAt(i);

			if (charExp == '(' || charExp == '[' || charExp == '{') {
				// Push element if matched with targeted Character
				stack.push(charExp);
				continue;
			}

			if (stack.isEmpty())
				return false;
			char check;
			switch (charExp) {
			case ')':
				check = stack.pop();
				if (check == '{' || check == '[')
					return false;
				break;

			case '}':
				check = stack.pop();
				if (check == '(' || check == '[')
					return false;
				break;

			case ']':
				check = stack.pop();
				if (check == '(' || check == '{')
					return false;
				break;
			}
		}

		// if stack empty means blanced
		return (stack.isEmpty());
	}
}
