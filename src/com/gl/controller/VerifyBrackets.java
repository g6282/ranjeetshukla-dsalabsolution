package com.gl.controller;

import com.gl.services.VerifyBracketsBalanced;

public class VerifyBrackets {

	public static void main(String[] args) {
		String brackets = "([[{}]])"; // "([[{}]]))";

		VerifyBracketsBalanced bracket = new VerifyBracketsBalanced();

		boolean isBalanced = bracket.isBracketsBalanced(brackets);
		if (isBalanced) {
			System.out.println("The entered String has Balanced Brackets");
		} else {
			System.out.println("The entered Strings do not contain Balanced Brackets");
		}
	}

}
